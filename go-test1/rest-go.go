package main

import (
	"github.com/gin-gonic/gin"
)

func returnHelloText(c *gin.Context) {
	c.String(200, "Hello world")
}

func main() {
	r := gin.Default()
	r.GET("/hello", returnHelloText)
	r.Run()
}
