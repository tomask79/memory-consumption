# REST Frameworks Memory Consumption Test #

In today's world of cloud solutions resources costs money. So choosing the resources efficient technology should be one of the first pieces of the puzzle when starting the project. 
To maybe help you a little with that I decided to do a very simple test. I'm going to create simple REST API application offering GET operation returning simple "Hello world".
And to make it fun and I'm going to create it in:

* Golang with help of GIN Framework
* Spring Boot 2.x
* Micronaut
* Spring Boot 2.6.6 with Spring Native

All four samples will be wrapped into Docker image and checked with docker stats for resource consumption, let's get started.

### Golang with help of GIN framework ###

My version of Go on my Mac is:

```
A0057:go-test1 tomas.kloucek$ go version
go version go1.17.2 darwin/amd64
```

Module initialization:

```
go mod init go-test1
```

Adding of GIN framework as dependency module:

```
go get -u github.com/gin-gonic/gin
```

Now simple GET operation on the /hello context returning simple text "Hello world"

```
package main

import (
	"github.com/gin-gonic/gin"
)

func returnHelloText(c *gin.Context) {
	c.String(200, "Hello world")
}

func main() {
	r := gin.Default()
	r.GET("/hello", returnHelloText)
	r.Run()
}
```

Coding done, Dockerfile:

```
FROM golang:alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

COPY *.go ./

RUN go mod download

RUN go build -o /docker-go-test

EXPOSE 8080

CMD [ "/docker-go-test" ]
```

build and run of docker image:

```
docker build --tag docker-go-test .
```

```
docker run -it -p 8080:8080 docker-go-test
```

If your want to verify functionality of course http://localhost:8080/hello, but that's not why we're here...

Docker stats of container running Go with GIN:

```
CONTAINER ID   NAME                 CPU %     MEM USAGE / LIMIT     MEM %     NET I/O     BLOCK I/O    PIDS
b039afdc9431   hardcore_sanderson   0.00%     4.121MiB / 1.941GiB   0.21%     876B / 0B   4.1kB / 0B   6
```

Container is having just **4MB** Very nice resource utitilization by Go!


### Spring Boot 2.x ###

All right, let's do the same thing with Spring Boot 2.5.5:

```
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.5.5</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	.
	.
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-web</artifactId>
	</dependency>
```

```
package com.example.demo.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String returnHelloText() {
        return "Hello world";
    }
}
```

Coding done, Dockerfile:

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

Build of Docker image (be in the root of boot2x-test2) :

```
docker build --tag docker-boot2x-test .
```

Run:

```
docker run -it -p 8080:8080 docker-boot2x-test
```

As in the first case you can verify http://localhost:8080/hello, but we're interested in docker stats:

```
CONTAINER ID   NAME              CPU %     MEM USAGE / LIMIT     MEM %     NET I/O     BLOCK I/O   PIDS
2e25f993140a   goofy_blackburn   0.59%     171.3MiB / 1.941GiB   8.62%     876B / 0B   0B / 0B     22
```

Container with Spring Boot 2.x under the hood doing the same thing eats **171MB**! Well that pretty much sucks compared to Go :\

### Micronaut ###

Same thing with Micronaut, version 2.5.5

```
  <parent>
    <groupId>io.micronaut</groupId>
    <artifactId>micronaut-parent</artifactId>
    <version>2.5.5</version>
  </parent>
```

REST controller:

```
  package com.memory.test.micronaut.controller;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller
public class HelloRestController {
    @Get("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String index() {
        return "Hello World";
    }
}
```

Coding done, Dockerfile same as with Spring Boot:

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

Build of Docker image:

```
docker build --tag docker-micronaut-test3 .
```

Start of the container:

```
docker run -it -p 8080:8080 docker-micronaut-test3
```

Output should be:

```
|  \/  (_) ___ _ __ ___  _ __   __ _ _   _| |_ 
| |\/| | |/ __| '__/ _ \| '_ \ / _` | | | | __|
| |  | | | (__| | | (_) | | | | (_| | |_| | |_ 
|_|  |_|_|\___|_|  \___/|_| |_|\__,_|\__,_|\__|
  Micronaut (v2.5.5)

19:19:48.313 [main] INFO  io.micronaut.runtime.Micronaut - Startup completed in 1858ms. Server Running: http://4d94b614df95:8080
```

Of course ping http://localhost:8080/hello, but docker stats is what matters now:

```
CONTAINER ID   NAME         CPU %     MEM USAGE / LIMIT     MEM %     NET I/O         BLOCK I/O   PIDS
4d94b614df95   brave_wing   0.46%     81.77MiB / 1.941GiB   4.11%     1.18kB / 168B   0B / 0B     18
```

With Micronaut we do ** 82MB ** which is almost twice better starting resource utilization compared to Spring Boot 2.x. 
[ASM](https://asm.ow2.io) under hood does a lot of great work.

### Spring Boot 2.6.6 with Spring Native ###

Let's have same controller as with vanilla Boot 2.5.5:

```
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String returnHelloText() {
        return "Hello world";
    }
}
```

with spring-native and spring-web on the classpath:

```
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.experimental</groupId>
			<artifactId>spring-native</artifactId>
			<version>${spring-native.version}</version>
		</dependency>
```
and spring's AOT and paketobuildpacks plugins

```
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<classifier>${repackage.classifier}</classifier>
					<image>
						<builder>paketobuildpacks/builder:tiny</builder>
						<env>
							<BP_NATIVE_IMAGE>true</BP_NATIVE_IMAGE>
						</env>
					</image>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.experimental</groupId>
				<artifactId>spring-aot-maven-plugin</artifactId>
				<version>${spring-native.version}</version>		
				
			.
			.
			.
```

Now build of Native Image:

```
mvn spring-boot:build-image
```

this will produce demo:0.0.1-SNAPSHOT docker image => let's start it:

```
A0057:spring-native tomas.kloucek$ docker run -it -p 8080:8080 demo:0.0.1-SNAPSHOT
2022-04-11 20:00:03.232  INFO 1 --- [           main] o.s.nativex.NativeListener               : AOT mode enabled

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.6.6)

2022-04-11 20:00:03.234  INFO 1 --- [           main] com.example.demo.DemoApplication         : Starting DemoApplication using Java 11.0.14.1 on 22372c3f496f with PID 1 (/workspace/com.example.demo.DemoApplication started by cnb in /workspace)
2022-04-11 20:00:03.234  INFO 1 --- [           main] com.example.demo.DemoApplication         : No active profile set, falling back to 1 default profile: "default"
2022-04-11 20:00:03.242  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2022-04-11 20:00:03.242  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-04-11 20:00:03.242  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.60]
2022-04-11 20:00:03.245  INFO 1 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-04-11 20:00:03.245  INFO 1 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 11 ms
2022-04-11 20:00:03.256  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2022-04-11 20:00:03.256  INFO 1 --- [           main] com.example.demo.DemoApplication         : Started DemoApplication in 0.029 seconds (JVM running for 0.03)
```

Of course 'docker stats' :

```
CONTAINER ID   NAME             CPU %     MEM USAGE / LIMIT     MEM %     NET I/O       BLOCK I/O   PIDS
22372c3f496f   hardcore_moore   0.02%     20.52MiB / 7.775GiB   0.26%     1.02kB / 0B   0B / 0B     17
```

Wow! Same application, but compiled into native image consumes just ** 20MB ** instead of ** 171MB ** like with vanilla Spring Boot.

And let me point out few points:

- Size of native image is just 88MB which is nice
- Impressive start up speed, check the time in the run window I provided

Spring Boot 3 is going to have Spring Native baked inside of it's starter. Which is another good news.


### Summary ###

Frameworks like Micronaut and Quarkus are a huge competitors for Spring Ecosystem but it seems with Spring 6.0 and native images 
VMware guys are on the good track. Even if Golang looks unbeatable here with incredibly fast growing community (I count myself there as well)
Java will probably survive for some time :)
